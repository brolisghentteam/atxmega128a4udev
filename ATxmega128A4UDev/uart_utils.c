
#include <stdint.h>
#include <util/delay.h>
#include <avr/io.h>

#include "uart_utils.h"


void init_uart() {

	/* ------ init USARTC0 ------ */
	PORTC.OUTSET = 0x08;	/* set TXD pin high			*/
	PORTC.DIRSET = 0x08;	/* set TXD pin as output	*/
	PORTC.OUTCLR = 0x04;	/* set RXD pin low			*/
	PORTC.DIRCLR = 0x04;	/* set RXD pin as input		*/

	USARTC0.CTRLC = 0x03;	/* 8-bit, no parity, 1 stop bit	*/

	/* set baud rate to 115200 */
	USARTC0.BAUDCTRLA = (uint8_t)(2094 & 0x00FF);
	USARTC0.BAUDCTRLB = (uint8_t)((2094 & 0x0F00) >> 8) | 0b10010000;	/* BSCALE = -7	*/

	/* enable receiver and transmitter */
	USARTC0.CTRLB = USART_RXEN_bm | USART_TXEN_bm;
}



void put_char(char c) {

	USARTC0.STATUS = USART_TXCIF_bm;	/* clear TXC flag	*/
	USARTC0.DATA = c;
	while (!(USARTC0.STATUS & USART_TXCIF_bm));	/* wait until char is transmitted */
}


void put_string(char* str) {

	uint8_t i = 0;

	while (str[i]) {
		put_char(str[i++]);
	}
}


char get_char(void) {
	
	while (!(USARTC0.STATUS & USART_RXCIF_bm));	/* wait for char */
	return USARTC0.DATA;
}

