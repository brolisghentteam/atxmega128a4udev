
#ifndef UART_UTILS_H
#define UART_UTILS_H

extern void init_uart();
extern void put_char(char c);
extern void put_string(char* str);
extern char get_char(void);

#endif